<?php
/* KCode Developers
 * Programador [Kevin Daniel Guzman Delgadillo]
 * 2017-08-1
 * Controlador para las funciones de creacion de mensaje y otros
**/
class Mensaje extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('mensaje_model');
		$this->load->helper('url_helper');
	}

	public function crear() {
		$this->mensaje_model->crear();
		redirect('/page/index');
	}
}