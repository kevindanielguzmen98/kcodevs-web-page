<?php
/* KCode Developers
 * Programador [Kevin Daniel Guzman Delgadillo]
 * 2017-07-28
 * Controlador principal para al pagina web kcodedevs
**/
class Page extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('url_helper');
	}

	public function index() {
		$this->load->view('principal/header', array('titulo' => 'KCode Developers'));
		$this->load->view('principal/homepage');
		$this->load->view('principal/footer');
	}
}
