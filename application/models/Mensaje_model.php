<?php 
/* KCode Developers
 * Programador [Kevin Daniel Guzman Delgadillo]
 * 2017-08-1
 * Modelo de gestion de mensaje
**/
class Mensaje_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

	public function crear() {
		$datos = array(
			'nombre'    => $this->input->post('nombre'),
			'email'     => $this->input->post('email'),
			'contenido' => $this->input->post('mensaje')
		);
		return $this->db->insert('mensaje', $datos);
	}
}