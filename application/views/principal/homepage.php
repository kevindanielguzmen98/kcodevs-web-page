<link rel="stylesheet" href="<?= base_url() ?>/recursos/css/home.css">
<div class="navbar-fixed">
    <nav>
        <div class="nav-wrapper grey darken-3">
            <a href="#" data-activates="menu-movil" class="button-collapse"><i class="medium material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down nav-centrado fixed">
                <li><a href="#" class="btn0">Principal</a></li>
                <li><a href="#" class="btn1">Quiénes somos</a></li>
                <li><a href="#" class="btn2">Proyectos</a></li>
                <li><a href="#" class="btn3">Contacto</a></li>
            </ul>
            <ul class="side-nav grey darken-3" id="menu-movil">
                <li><a class="white-text center-align btn0" href="#">Principal</a></li>
                <li><a class="white-text center-align btn1" href="#">Quiénes somos</a></li>
                <li><a class="white-text center-align btn2" href="#">Proyectos</a></li>
                <li><a class="white-text center-align btn3" href="#">Contacto</a></li>
            </ul>
        </div>
    </nav>
</div>
<main>
    <div id="imagen-parallax" class="parallax-container">
        <div class="container white-text">
            <div class="row center">
                <div class="col l10 s12 offset-l1">
                    <br><br><br>
                    <img src="<?= base_url() ?>/recursos/imagenes/titulo-kcodedevs.png" class="img-logo-titulo" alt="">
                    <br>
                    <h4>Soluciones de software para pequeñas, medianas y grandes empresas</h4>
                </div>
            </div>
        </div>
        <div class="parallax">
            <img src="<?= base_url() ?>/recursos/imagenes/img-1.jpg" alt="">
        </div>
    </div>
    <div class="container" id="pos1">
        <div class="section">
            <div class="row">
                <div class="col l10 s12 offset-l1 center-align">
                    <br>
                    <h4>¿Quiénes somos?</h4>
                    <br><br>
                    <p class="flow-text">Somos una empresa de desarrollo de software a medida de pequeña y grande envergadura,
                       con una amplia acojida de todos los lineamientos de calidad y documentación de software.
                       Nuestro equipo esta en constante crecimiento con integrandes jovenes que son capaces de
                       brindar grandes ideas para el crecimiento de su proyecto. Estamos listos para acompañarlo en todo lo que necesite.
                       <br><br><br>
                       <a href="#" class="btn teal lighten-1"><strong>¡Su proyecto tambien es nuestro proyecto!</strong></a>
                   </p>
                </div>
            </div>
        </div>
    </div>
    <div id="imagen-parallax" class="parallax-container parallax-2">
        <div class="container white-text">
            <div class="row center">
                <div class="col l10 s12 offset-l1">
                    <br><br><br><br><br><br>
                    <h4>Te acompañaremos en todas las etapas de tu proyecto de software</h4>
                </div>
            </div>
        </div>
        <div class="parallax">
            <img src="<?= base_url() ?>/recursos/imagenes/img-2.jpg" alt="">
        </div>
    </div>
    <div class="container" id="pos2">
        <div class="section">
            <div class="row">
                <div class="col l10 s12 offset-l1 center-align">
                    <br>
                    <h4>Nuestros proyectos</h4>
                </div>
            </div>
            <div class="row">
                <div class="col l4 s12 m6 offset-m3 center-align grey lighten-3 padding-elemento">
                    <i class="material-icons medium">assignment_turned_in</i>
                    <h5>Terminados</h5>
                    <div class="divider"></div>
                    <br>
                    <table>
                        <tr>
                            <td class="center-align" colspan="2">
                                Estamos trabajando en ello
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col l4 s12 m6 offset-m3 center-align grey lighten-3 padding-elemento">
                    <i class="material-icons medium">build</i>
                    <h5>En Desarrollo</h5>
                    <div class="divider"></div>
                    <br>
                    <table class="white left">
                        <tr>
                            <td class="center" colspan="2">
                                <p><strong>Ventas rafa</strong></p>
                                <a href="#" class="center btn" title="Ver"><i class="material-icons left">visibility</i>Ver</a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col l4 s12 m6 offset-m3 center-align grey lighten-3 padding-elemento">
                    <i class="material-icons medium">collections_bookmark</i>
                    <h5>A Futuro</h5>
                    <div class="divider"></div>
                    <br>
                    <table>
                        <tr>
                            <td class="center-align" colspan="2">
                                <a href="#"><i class="material-icons medium">add</i><br>Pon tu proyecto aqui</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<main class="grey lighten-3">
    <div class="container" id="pos3">
        <div class="row">
            <div class="col l10 s12 offset-l1 center-align">
                <br><br><br>
                <h4>Contáctate con nosotros</h4>
            </div>
            <div class="col l10 s12 offset-l1 center-align">
                <div class="card">
                    <div class="card-content">
                        <div class="row">
                            <form class="col s12" action="<?= site_url('mensaje/c'); ?>" method="post">
                                <div class="row">
                                    <div class="input-field col s6">
                                        <input type="text" name="nombre" id="txtNombre" value="">
                                        <label for="txtNombre">Nombre</label>
                                    </div>
                                    <div class="input-field col s6">
                                        <input type="email" name="email" id="txtEmail" value="">
                                        <label for="txtEmail">Email</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea name="mensaje" id="txtareaMensaje" class="materialize-textarea"></textarea>
                                        <label for="txtareaMensaje">Mensaje</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col s12 center-align">
                                        <input type="submit" value="Enviar" class="btn">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<footer class="page-footer grey darken-3">
    <div class="container">
        <div class="row">
            <div class="col l10 s12 offset-l1">
                <h5 class="white-text">KCode Developers</h5>
                <br>
                <ul>
                    <li>Ibague Tolima, Cra 20 # 16 - 07 B. Clarita Botero</li>
                    <li>Cel: +57 3183995668</li>
                    <li>Skype: kevindanielguzmen98_1</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright grey darken-4">
        <div class="container">
            <div class="row">
                <div class="col l6 s12 center-align">
                    This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
                </div>
                <div class="col l6 s12 right-align">
                    <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript">
    $(document).ready(function() {
        $('.parallax').parallax();
        $('.button-collapse').sideNav();
        $('a[href=#]').click(function(e) {
            e.preventDefault();
        });
        $('.btn0').click(function() {
            $('html body').animate({
                scrollTop: 0
            }, 1000);
        });
        $('.btn1').click(function() {
            let position = $('#pos1').offset().top;
            $('html body').animate({
                scrollTop: position - 150
            }, 1000);
        });
        $('.btn2').click(function() {
            let position = $('#pos2').offset().top;
            $('html body').animate({
                scrollTop: position - 150
            }, 1000);
        });
        $('.btn3').click(function() {
            let position = $('#pos3').offset().top;
            $('html body').animate({
                scrollTop: position - 100
            }, 1000);
        });
    });
</script>
