<!DOCTYPE html>
<html lang="es">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" charset="UTF-8">
	<title><?= $titulo ?></title>
	<!-- Librerias de fuentes -->
	<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<!-- Libreria de estilos -->
	<link rel="stylesheet" href="<?= base_url() ?>recursos/librerias/materialize/css/materialize.min.css">
	<!-- Libreria jquery -->
	<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>recursos/librerias/materialize/js/materialize.min.js"></script>
</head>
<body>